use brainfucks::interpret::run_interactive;
use brainfucks::parse::parse_program;
use std::error;

fn main() -> Result<(), Box<dyn error::Error>> {
    let text = include_str!("../input/mandelbrot.b");
    let program = parse_program(text)?;

    run_interactive(program);

    Ok(())
}
