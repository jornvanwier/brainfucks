use crate::instruction::Instruction;

const BOILERPLATE: &str = include_str!("boilerplate.s");
const STDOUT: &str = "
mov rax, SYS_READ
mov rdi, STDIN
mov rsi, r12
mov rdx, 1
syscall
";
const STDIN: &str = "
mov rax, SYS_WRITE
mov rdi, STDOUT
mov rsi, r12
mov rdx, 1
syscall
";

macro_rules! start_loop_fmt {
    () => {
        "
cmpb [r12], 0
je LOOP_END_{}
LOOP_START_{}:
"
    };
}

macro_rules! end_loop_fmt {
    () => {
        "
cmpb [r12], 0
jne LOOP_START_{}
LOOP_END_{}:
"
    };
}

pub fn compile(program: &[Instruction]) -> String {
    let mut boilerplate = BOILERPLATE.split("#{{REPLACE}}");
    let header = boilerplate.next().unwrap();
    let footer = boilerplate.next().unwrap();

    let assembly = program
        .iter()
        .enumerate()
        .map(|(idx, instruction)| match instruction {
            Instruction::IncPtr(rle) => format!("add r12, {}", rle),
            Instruction::DecPtr(rle) => format!("sub r12, {}", rle),
            Instruction::IncValue(rle) => format!("addb [r12], {}", rle),
            Instruction::DecValue(rle) => format!("subb [r12], {}", rle),
            Instruction::Output(rle) => STDIN.repeat(*rle),
            Instruction::Input(rle) => STDOUT.repeat(*rle),
            Instruction::OpenLoop(_rle, close) => format!(start_loop_fmt!(), close - 1, idx),
            Instruction::CloseLoop(_rle, open) => format!(end_loop_fmt!(), open - 1, idx),
        })
        .collect::<Vec<String>>()
        .join("\n");

    vec![header, &assembly, footer].join("\n")
}
