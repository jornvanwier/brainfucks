use crate::instruction::Instruction;
use crate::utils;
use utils::indent;

const BOILERPLATE: &str = include_str!("boilerplate.py");

pub fn transpile_to_string(program: &[Instruction]) -> String {
    let mut content = transpile(program);
    content.insert(0, BOILERPLATE.into());
    content.join("\n")
}

fn transpile(program: &[Instruction]) -> Vec<String> {
    let mut components = vec![];
    let mut indentation = 0;
    let mut idx = 0;

    while idx < program.len() {
        let instruction = program[idx];

        let code = match instruction {
            Instruction::IncPtr(rle) => indent(indentation, format!("data_ptr += {}", rle)),
            Instruction::DecPtr(rle) => indent(indentation, format!("data_ptr -= {}", rle)),
            Instruction::IncValue(rle) => {
                indent(indentation, format!("memory[data_ptr] += {}", rle))
            }
            Instruction::DecValue(rle) => {
                indent(indentation, format!("memory[data_ptr] -= {}", rle))
            }
            Instruction::Output(rle) => {
                indent(indentation, format!("write(memory[data_ptr], {})", rle))
            }
            Instruction::Input(rle) => indent(indentation, format!("sys.stdin.read({})", rle)),
            Instruction::OpenLoop(rle, _) => {
                let old_indentation = indentation;
                indentation += rle;
                (0..rle)
                    .map(|extra_indent| {
                        indent(old_indentation + extra_indent, "while memory[data_ptr]:")
                    })
                    .fold(String::new(), |a, b| a + &b + "\n")
            }
            Instruction::CloseLoop(rle, _) => {
                indentation -= rle;
                "".into()
            }
        };
        components.push(code);

        idx += 1;
    }

    components
}
