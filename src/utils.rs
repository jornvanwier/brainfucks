pub fn indent<S: AsRef<str>>(n: usize, s: S) -> String {
    format!("{}{}", " ".repeat(n * 4), s.as_ref())
}
