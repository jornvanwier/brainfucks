use std::fmt;
use std::{error, mem};

pub const VALID: &[char] = &['>', '<', '+', '-', '.', ',', '[', ']'];

// Instructions are encoded with the run length. The second usize for the jumps are the indices of
// the instruction after their matching counterpart.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Instruction {
    IncPtr(usize),
    DecPtr(usize),
    IncValue(usize),
    DecValue(usize),
    Output(usize),
    Input(usize),
    OpenLoop(usize, usize),
    CloseLoop(usize, usize),
}

impl Instruction {
    pub fn increment_rle(&mut self) {
        match self {
            Instruction::IncPtr(n) => *n += 1,
            Instruction::DecPtr(n) => *n += 1,
            Instruction::IncValue(n) => *n += 1,
            Instruction::DecValue(n) => *n += 1,
            Instruction::Output(n) => *n += 1,
            Instruction::Input(n) => *n += 1,
            Instruction::OpenLoop(n, _) => *n += 1,
            Instruction::CloseLoop(n, _) => *n += 1,
        }
    }

    pub fn same_variant(&self, other: &Instruction) -> bool {
        mem::discriminant(self) == mem::discriminant(other)
    }
}

#[derive(Debug, Clone)]
pub struct UnknownInstructionError(char);

impl fmt::Display for UnknownInstructionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Could not parse {} as an instruction", self.0)
    }
}

impl error::Error for UnknownInstructionError {}

impl From<char> for Instruction {
    fn from(value: char) -> Self {
        use Instruction::*;
        match value {
            '>' => IncPtr(1),
            '<' => DecPtr(1),
            '+' => IncValue(1),
            '-' => DecValue(1),
            '.' => Output(1),
            ',' => Input(1),
            '[' => OpenLoop(1, 0),
            ']' => CloseLoop(1, 0),
            _ => unreachable!(),
        }
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::Instruction::*;
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn test_same_variant() {
        assert!(IncPtr(1).same_variant(&IncPtr(1)));
        assert!(IncPtr(1).same_variant(&IncPtr(2)));
        assert!(!(IncPtr(1).same_variant(&DecPtr(1))));
    }
}
