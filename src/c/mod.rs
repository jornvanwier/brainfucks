use crate::instruction::Instruction;
use crate::utils::indent;

const BOILERPLATE: &str = include_str!("boilerplate.c");

pub fn transpile(program: &[Instruction]) -> String {
    let mut boilerplate = BOILERPLATE.split("//{{REPLACE}}");
    let header = boilerplate.next().unwrap();
    let footer = boilerplate.next().unwrap();

    let mut indentation = 1;

    let code = program
        .iter()
        .map(|instruction| match instruction {
            Instruction::IncPtr(rle) => indent(indentation, format!("data_ptr += {};", rle)),
            Instruction::DecPtr(rle) => indent(indentation, format!("data_ptr -= {};", rle)),
            Instruction::IncValue(rle) => indent(indentation, format!("*data_ptr += {};", rle)),
            Instruction::DecValue(rle) => indent(indentation, format!("*data_ptr -= {};", rle)),
            Instruction::Output(rle) => indent(indentation, "putchar(*data_ptr);".repeat(*rle)),
            Instruction::Input(rle) => indent(indentation, "*data_ptr=getchar();".repeat(*rle)),
            Instruction::OpenLoop(rle, _close) => {
                let old_indentation = indentation;
                indentation += rle;
                (0..*rle)
                    .map(|extra_indent| {
                        indent(old_indentation + extra_indent, "while (*data_ptr) {")
                    })
                    .fold(String::new(), |a, b| a + &b + "\n")
            }
            Instruction::CloseLoop(rle, _open) => {
                let old_indentation = indentation;
                indentation -= rle;
                (0..*rle)
                    .map(|extra_indent| indent(old_indentation - extra_indent, "}"))
                    .fold(String::new(), |a, b| a + &b + "\n")
            }
        })
        .collect::<Vec<String>>()
        .join("\n");

    vec![header, &code, footer].join("\n")
}
