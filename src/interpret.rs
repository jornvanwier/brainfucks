use std::io;
use std::io::{Bytes, Read, Write};

use crate::instruction::Instruction;

const MEM_SIZE: usize = 30_000;

pub fn run_interactive(program: Vec<Instruction>) {
    let input = io::stdin();
    let input = input.lock().bytes();
    let output = io::stdout();
    let mut output = output.lock();

    run(program, input, &mut output);
}

pub fn run<R: Read, W: Write>(program: Vec<Instruction>, mut input: Bytes<R>, output: &mut W) {
    let mut memory = [0u8; MEM_SIZE];
    let mut data_pointer = 0;
    let mut instruction_pointer = 0;

    while instruction_pointer < program.len() {
        let instruction = program[instruction_pointer];
        match instruction {
            Instruction::IncPtr(rle) => data_pointer += rle,
            Instruction::DecPtr(rle) => data_pointer -= rle,
            Instruction::IncValue(rle) => {
                memory[data_pointer] = memory[data_pointer].wrapping_add(rle as u8);
            }
            Instruction::DecValue(rle) => {
                memory[data_pointer] = memory[data_pointer].wrapping_sub(rle as u8);
            }
            Instruction::Output(rle) => {
                let mut buffer = vec![0; rle];
                io::repeat(memory[data_pointer])
                    .read_exact(&mut buffer)
                    .unwrap();
                output.write(&buffer).expect("Could not write to buffer!");
            }
            Instruction::Input(rle) => {
                if let Err(error) = output.flush() {
                    panic!("error while flushing to output: {}", error);
                }
                for _ in 0..rle {
                    match input.next() {
                        Some(Ok(byte)) => memory[data_pointer] = byte,
                        Some(Err(error)) => {
                            panic!("error while trying to read byte from input {}", error);
                        }
                        None => {
                            memory[data_pointer] = 0;
                        }
                    }
                }
            }
            Instruction::OpenLoop(_rle, close) => {
                if memory[data_pointer] == 0 {
                    instruction_pointer = close;
                } else {
                    instruction_pointer += 1;
                }
            }
            Instruction::CloseLoop(_rle, open) => {
                if memory[data_pointer] != 0 {
                    instruction_pointer = open;
                } else {
                    instruction_pointer += 1;
                }
            }
        }

        match instruction {
            Instruction::IncPtr(_)
            | Instruction::DecPtr(_)
            | Instruction::IncValue(_)
            | Instruction::DecValue(_)
            | Instruction::Output(_)
            | Instruction::Input(_) => instruction_pointer += 1,
            Instruction::OpenLoop(_, _) | Instruction::CloseLoop(_, _) => {}
        }
    }
}

mod tests {
    #[allow(unused_imports)]
    use super::*;
    #[allow(unused_imports)]
    use crate::parse::parse_program;

    #[test]
    fn test_hello_world() {
        let text = include_str!("../input/hello_world.b");
        let program = parse_program(text).unwrap();

        let input = [].bytes();
        let mut output = Vec::new();

        run(program, input, &mut output);
        let result = std::str::from_utf8(&output).unwrap();
        assert_eq!(result, "Hello World!\n")
    }
}
