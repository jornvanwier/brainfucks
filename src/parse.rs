use std::{error, fs};

use crate::instruction::{Instruction, VALID};

pub fn parse_program<T: AsRef<str>>(text: T) -> Result<Vec<Instruction>, Box<dyn error::Error>> {
    let text = text.as_ref();

    let mut result = vec![];
    let mut open_stack = vec![];
    let mut chars = text.chars().filter(|c| VALID.contains(c)).peekable();

    if chars.peek().is_none() {
        return Ok(result);
    }

    let mut previous_instruction = None;

    for character in chars {
        let mut instruction = Instruction::from(character);

        if let Some(mut prev) = previous_instruction {
            if instruction.same_variant(&prev) {
                prev.increment_rle();
                instruction = prev;
            } else {
                push_instruction(&mut result, &mut open_stack, prev);
            }
        }

        if let Instruction::OpenLoop(_, _) = instruction {
            open_stack.push(result.len())
        }

        previous_instruction = Some(instruction);
    }

    // Make sure to write the last character
    push_instruction(
        &mut result,
        &mut open_stack,
        previous_instruction.expect("Should have at least one expression at the end!"),
    );

    Ok(result)
}

fn push_instruction(
    result: &mut Vec<Instruction>,
    open_stack: &mut Vec<usize>,
    previous_instruction: Instruction,
) {
    let mut instruction = previous_instruction;

    if let Instruction::CloseLoop(close_rle, _) = instruction {
        // Make sure we only link the closing instruction once
        let mut closed = false;

        for _ in 0..close_rle {
            let open_idx = open_stack.pop().expect("Unmatched closing bracket!");
            if !closed {
                instruction = Instruction::CloseLoop(close_rle, open_idx + 1);
                closed = true;
            }
            if let Instruction::OpenLoop(open_rle, _) = result[open_idx] {
                result[open_idx] = Instruction::OpenLoop(open_rle, result.len() + 1)
            } else {
                panic!("Misaligned open stack!")
            }
        }
    }

    result.push(instruction);
}

pub fn parse_file<T: AsRef<str>>(file_name: T) -> Result<Vec<Instruction>, Box<dyn error::Error>> {
    let text = fs::read_to_string(file_name.as_ref())?;

    parse_program(text)
}

mod tests {
    #[allow(unused_imports)]
    use super::Instruction::*;
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn parse_empty() {
        let instructions = parse_program("").unwrap();
        assert_eq!(&instructions, &[]);
    }

    #[test]
    fn parse_inc_ptr() {
        let instructions = parse_program(">").unwrap();
        assert_eq!(&instructions, &[IncPtr(1)]);
    }

    #[test]
    fn parse_inc_ptrs() {
        let instructions = parse_program(">>>").unwrap();
        assert_eq!(&instructions, &[IncPtr(3)]);
    }

    #[test]
    fn parse_dec_ptr() {
        let instructions = parse_program("<").unwrap();
        assert_eq!(&instructions, &[DecPtr(1)]);
    }

    #[test]
    fn parse_dec_ptrs() {
        let instructions = parse_program("<<<").unwrap();
        assert_eq!(&instructions, &[DecPtr(3)]);
    }

    #[test]
    fn parse_inc_byte() {
        let instructions = parse_program("+").unwrap();
        assert_eq!(&instructions, &[IncValue(1)]);
    }

    #[test]
    fn parse_inc_bytes() {
        let instructions = parse_program("+++").unwrap();
        assert_eq!(&instructions, &[IncValue(3)]);
    }

    #[test]
    fn parse_dec_byte() {
        let instructions = parse_program("-").unwrap();
        assert_eq!(&instructions, &[DecValue(1)]);
    }

    #[test]
    fn parse_dec_bytes() {
        let instructions = parse_program("---").unwrap();
        assert_eq!(&instructions, &[DecValue(3)]);
    }

    #[test]
    fn parse_read_byte() {
        let instructions = parse_program(",").unwrap();
        assert_eq!(&instructions, &[Input(1)]);
    }

    #[test]
    fn parse_read_bytes() {
        let instructions = parse_program(",,,").unwrap();
        assert_eq!(&instructions, &[Input(3)]);
    }

    #[test]
    fn parse_write_byte() {
        let instructions = parse_program(".").unwrap();
        assert_eq!(&instructions, &[Output(1)]);
    }

    #[test]
    fn parse_write_bytes() {
        let instructions = parse_program("...").unwrap();
        assert_eq!(&instructions, &[Output(3)]);
    }

    #[test]
    fn parse_loop() {
        let instructions = parse_program("[-]").unwrap();
        assert_eq!(
            &instructions,
            &[OpenLoop(1, 3), DecValue(1), CloseLoop(1, 1),],
        );
    }

    #[test]
    fn parse_loops() {
        let instructions = parse_program("[[[-]]]").unwrap();
        assert_eq!(
            &instructions,
            &[OpenLoop(3, 3), DecValue(1), CloseLoop(3, 1),],
        );
    }

    #[test]
    fn parse_all() {
        let instructions = parse_program("[><+-,.]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(1, 8),
                IncPtr(1),
                DecPtr(1),
                IncValue(1),
                DecValue(1),
                Input(1),
                Output(1),
                CloseLoop(1, 1),
            ],
        );
    }

    #[test]
    fn parse_alls() {
        let instructions = parse_program("[[[>>><<<+++---,,,...]]]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(3, 8),
                IncPtr(3),
                DecPtr(3),
                IncValue(3),
                DecValue(3),
                Input(3),
                Output(3),
                CloseLoop(3, 1),
            ],
        );
    }

    #[test]
    fn parse_uneven_loop_starts() {
        let instructions = parse_program("[>[>[-]]]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(1, 7),
                IncPtr(1),
                OpenLoop(1, 7),
                IncPtr(1),
                OpenLoop(1, 7),
                DecValue(1),
                CloseLoop(3, 5),
            ],
        );
    }

    #[test]
    fn parse_uneven_loop_ends() {
        let instructions = parse_program("[[[-]>]>]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(3, 7),
                DecValue(1),
                CloseLoop(1, 1),
                IncPtr(1),
                CloseLoop(1, 1),
                IncPtr(1),
                CloseLoop(1, 1),
            ],
        );
    }

    #[test]
    fn parse_uneven_loops_1() {
        let instructions = parse_program("[[[>[[-]>]>]]>]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(3, 11), // 0
                IncPtr(1),
                OpenLoop(2, 7), // 2
                DecValue(1),
                CloseLoop(1, 3), // 4
                IncPtr(1),
                CloseLoop(1, 3), // 6
                IncPtr(1),
                CloseLoop(2, 1), // 8
                IncPtr(1),
                CloseLoop(1, 1), // 10
            ],
        );
    }

    #[test]
    fn parse_uneven_loops_2() {
        let instructions = parse_program("[>[[>[>[[-]>]]>]]]").unwrap();
        assert_eq!(
            &instructions,
            &[
                OpenLoop(1, 13), // 0
                IncPtr(1),
                OpenLoop(2, 13), // 2
                IncPtr(1),
                OpenLoop(1, 11), // 4
                IncPtr(1),
                OpenLoop(2, 11), // 6
                DecValue(1),
                CloseLoop(1, 7), // 8
                IncPtr(1),
                CloseLoop(2, 7), // 10
                IncPtr(1),
                CloseLoop(3, 3), // 12
            ],
        );
    }
}
