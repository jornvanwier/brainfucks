use std::ffi::{OsStr, OsString};
use std::fs;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::{error, io};

use clap::{App, Arg};

use brainfucks::interpret::run_interactive;
use brainfucks::parse::parse_file;
use brainfucks::{c, python, x86_64};

fn main() -> Result<(), Box<dyn error::Error>> {
    let matches = App::new("My Super Program")
        .version("1.0")
        .author("Jorn van Wier <mail@jornvanwier.com>")
        .about("Hurts the brain, but is pretty cool yes")
        .arg(Arg::with_name("target").required(true).possible_values(&[
            "interpret",
            "python",
            "c",
            "x86_64",
        ]))
        .arg(
            Arg::with_name("action")
                .required(true)
                .possible_values(&["build", "run"]),
        )
        .arg(
            Arg::with_name("output")
                .help("Directory of resulting program")
                .long("output")
                .short("o")
                .takes_value(true),
        )
        .arg(Arg::with_name("input").help("Brainfuck file to read"))
        .get_matches();

    let input_file = match matches.value_of("input") {
        Some(input_file) => input_file.into(),
        None => {
            println!("No input specified, enter path:");
            let mut buf = String::new();
            io::stdin().read_line(&mut buf)?;
            // Remove newline at end
            buf.pop();
            buf
        }
    };

    let program = parse_file(&input_file)?;

    let target = matches.value_of("target").unwrap();
    let action = matches.value_of("action").unwrap();
    let output_file = if target == "interpret" {
        None
    } else {
        Some(
            matches
                .value_of("output")
                .ok_or("Output is required when compiling")?,
        )
    };

    let mut compiled = None;
    match target {
        "interpret" => {
            if action == "build" {
                panic!("Build is not a valid action for interpret")
            }
            run_interactive(program)
        }
        "python" => {
            compiled = Some(python::transpile_to_string(&program));
        }
        "x86_64" => {
            compiled = Some(x86_64::compile(&program));
        }
        "c" => {
            compiled = Some(c::transpile(&program));
        }
        _ => unreachable!(),
    }

    if let (Some(output_dir), Some(compiled)) = (output_file, compiled) {
        let name_base: OsString = Path::new(&input_file).file_stem().unwrap().into();

        let extension = match target {
            "python" => ".py",
            "x86_64" => ".s",
            "c" => ".c",
            _ => unreachable!(),
        };

        let output_file_path = create_file_path(output_dir, &name_base, extension);

        // Write output
        fs::write(&output_file_path, compiled)?;

        if action == "run" {
            match target {
                "python" => {
                    Command::new("python").arg(output_file_path).status()?;
                }
                "x86_64" => {
                    let object_file_path = create_file_path(output_dir, &name_base, ".o");

                    let mut output_flag = OsString::from("-o");
                    output_flag.push(&object_file_path);

                    // Assemble
                    let assemble_status = Command::new("as")
                        .arg(output_flag)
                        .arg(output_file_path)
                        .status()?;

                    if !assemble_status.success() {
                        Err(format!("Assembly failed with code {}", assemble_status))?;
                    }

                    // Link
                    let executable_file_path = create_file_path(output_dir, &name_base, "");
                    let link_status = Command::new("ld")
                        // .arg(ld_output_option)
                        .arg("-o")
                        .arg(&executable_file_path)
                        .arg(&object_file_path)
                        .status()?;

                    if !link_status.success() {
                        Err(format!("Linking failed with code {}", assemble_status))?;
                    }

                    // Execute
                    Command::new(&executable_file_path).status()?;
                }
                "c" => {
                    let executable_file_path = create_file_path(output_dir, &name_base, "");

                    // Compile
                    let mut output_flag = OsString::from("-o");
                    output_flag.push(&executable_file_path);
                    let compile_status = Command::new("gcc")
                        .arg("-O3")
                        .arg(output_flag)
                        .arg(output_file_path)
                        .status()?;

                    if !compile_status.success() {
                        Err(format!("Compiling failed with code {}", compile_status))?;
                    }

                    Command::new(executable_file_path).status()?;
                }
                _ => {}
            }
        }
    }

    io::stdout().flush()?;
    Ok(())
}

fn create_file_path(folder: &str, file_stem: &OsStr, extension: &str) -> PathBuf {
    let mut file_name = OsString::from(file_stem);
    file_name.push(extension);
    Path::new(folder).join(file_name)
}
